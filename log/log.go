package log

import (
	"os"

	logging "github.com/op/go-logging"
)

var (
	Log    = logging.MustGetLogger("API")
	format = logging.MustStringFormatter("%{color}%{time:15:04:05} %{shortfunc} - %{level} %{message}")
)

func InitializeLogging() {
	consoleLog := logging.NewLogBackend(os.Stderr, "", 0)
	consoleLogFormatter := logging.NewBackendFormatter(consoleLog, format)

	logging.SetBackend(consoleLogFormatter)
}
