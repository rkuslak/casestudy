package routes

import (
	"case/log"
	"case/models"
	repos "case/repository"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// Router contains state for routes and initializes routing
type Router struct {
	*mux.Router
	resultsRepo repos.ResultsProvider
}

func (r *Router) getProducts(res http.ResponseWriter, req *http.Request) {
	repo := r.resultsRepo
	routeVars := mux.Vars(req)
	itemID, err := strconv.Atoi(routeVars["id"])
	if err != nil || itemID < 1 {
		res.WriteHeader(http.StatusNotFound)
		return
	}

	response, err := repo.GetPriceForItemID(itemID)
	if err != nil {
		res.WriteHeader(http.StatusBadRequest)
	}

	json.NewEncoder(res).Encode(&response)
}

func (r *Router) postProducts(res http.ResponseWriter, req *http.Request) {
	// TODO: Do we need to test that we get a result from the redsky repo for item?

	var itemPrice models.Price

	routeVars := mux.Vars(req)
	itemID, err := strconv.Atoi(routeVars["id"])
	if err != nil || itemID < 1 {
		log.Log.Error("Request ID passed is invalid")
		res.WriteHeader(http.StatusNotFound)
		return
	}

	if json.NewDecoder(req.Body).Decode(&itemPrice) != nil {
		log.Log.Error("Request body passed is invalid")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	if itemPrice.Currency == "" || itemPrice.Value == 0 {
		log.Log.Error("Request body passed is invalid; 0 value request")
		res.WriteHeader(http.StatusBadRequest)
		return
	}

	if r.resultsRepo.SetPriceForItemID(itemID, itemPrice.Value, itemPrice.Currency) == nil {
		item, err := r.resultsRepo.GetPriceForItemID(itemID)
		if err == nil {
			json.NewEncoder(res).Encode(&item)
			return
		}
	}

	res.WriteHeader(http.StatusNotFound)
}

// NewRouter creates a router based on the passed repository
func NewRouter(resultsRepo *repos.ResultsRepository) *Router {
	r := mux.NewRouter()
	repo := &Router{
		r,
		resultsRepo,
	}

	r.HandleFunc("/products/{id}", repo.getProducts).Methods("GET")
	r.HandleFunc("/products/{id}", repo.postProducts).Methods("POST")

	return repo
}
