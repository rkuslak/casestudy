package routes

import (
	"bytes"
	"case/log"
	"case/models"
	"case/repository"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func init() {
	log.InitializeLogging()
}

func Test_getProducts(t *testing.T) {
	redSkyRepo := repository.NewRedSkyRepo()
	itemsRepo := repository.NewItemPriceRepository("mongodb://root:superSecret@localhost:27017")
	resultRepo := repository.NewResultsProvider(itemsRepo, redSkyRepo)
	routes := NewRouter(resultRepo)

	type TestCase struct {
		name   string
		url    string
		wanted int
	}

	testCases := []TestCase{
		TestCase{"Good item ID", "/products/13860428", http.StatusOK},
		TestCase{"Bad item ID", "/products/13860422", http.StatusBadRequest},
	}

	for _, test := range testCases {
		req, _ := http.NewRequest("GET", test.url, nil)
		res := httptest.NewRecorder()
		routes.ServeHTTP(res, req)
		if res.Code != test.wanted {
			t.Errorf("%s: Not response required - %v wanted %v", test.name, res.Code, test.wanted)
		}
	}
}

func Test_postProducts(t *testing.T) {
	redSkyRepo := repository.NewRedSkyRepo()
	itemsRepo := repository.NewItemPriceRepository("mongodb://root:superSecret@localhost:27017")
	resultRepo := repository.NewResultsProvider(itemsRepo, redSkyRepo)
	routes := NewRouter(resultRepo)

	type TestCase struct {
		itemID   int
		price    float64
		currency string
		wanted   int
	}

	testCases := []TestCase{
		TestCase{13860428, 20.00, "CAD", http.StatusOK},
		TestCase{13860428, 10.00, "USD", http.StatusOK},
	}

	for _, test := range testCases {
		url := fmt.Sprintf("/products/%d", test.itemID)
		jsonValue, _ := json.Marshal(&models.Price{Value: test.price, Currency: test.currency})
		body := bytes.NewBuffer(jsonValue)
		req, _ := http.NewRequest("POST", url, body)
		res := httptest.NewRecorder()
		routes.ServeHTTP(res, req)
		if res.Code != test.wanted {
			t.Errorf("Not response required")
		}

		if res.Code == http.StatusOK {
			item, err := resultRepo.GetPriceForItemID(test.itemID)
			if err != nil || test.price != item.CurrentPrice.Value {
				t.Errorf("postProducts: EXPECTED %f - GOT %f", test.price, item.CurrentPrice.Value)
			}
			if test.currency != strings.ToUpper(item.CurrentPrice.Currency) {
				t.Errorf("postProducts: EXPECTED %s - GOT %s", test.currency, item.CurrentPrice.Currency)
			}
		}
	}
}
