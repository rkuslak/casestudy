BUILD & RUN
===
go build
"case" executable will be available in main directory
* `GET`: localhost/products/{product_id} to get a item if a price has been established
* `POST`: localhost/products/{product_id} to set a item price and currency. 

Expects JSON body in `POST` requests:
```JSON
{
  "value": float64 for price of product,
  "currency-code": string of the currency for the product,
}
```
If item does not have a record on POST, record will be created.

MongoDB backend
===
Docker Compose will provide on local host and expose needed ports