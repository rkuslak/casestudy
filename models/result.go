package models

type Price struct {
	Value    float64 `json:"value"`
	Currency string  `json:"currency_code"`
}
type Result struct {
	ID           int    `json:"id"`
	Name         string `json:"name"`
	CurrentPrice Price  `json:"current_price"`
}
