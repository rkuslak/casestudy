package models

import "time"

type Labels struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Type     string `json:"type"`
	Priority int    `json:"priority"`
	Count    int    `json:"count"`
}

type DeepRedLabels struct {
	TotalCount int      `json:"total_count"`
	Labels     []Labels `json:"labels"`
}

type AvailableToPromiseNetwork struct {
	ProductID                        string        `json:"product_id"`
	IDType                           string        `json:"id_type"`
	AvailableToPromiseQuantity       int           `json:"available_to_promise_quantity"`
	StreetDate                       time.Time     `json:"street_date"`
	Availability                     string        `json:"availability"`
	OnlineAvailableToPromiseQuantity int           `json:"online_available_to_promise_quantity"`
	StoresAvailableToPromiseQuantity int           `json:"stores_available_to_promise_quantity"`
	AvailabilityStatus               string        `json:"availability_status"`
	MultichannelOptions              []interface{} `json:"multichannel_options"`
	IsInfiniteInventory              bool          `json:"is_infinite_inventory"`
	LoyaltyAvailabilityStatus        string        `json:"loyalty_availability_status"`
	LoyaltyPurchaseStartDateTime     time.Time     `json:"loyalty_purchase_start_date_time"`
	IsLoyaltyPurchaseEnabled         bool          `json:"is_loyalty_purchase_enabled"`
	IsOutOfStockInAllStoreLocations  bool          `json:"is_out_of_stock_in_all_store_locations"`
	IsOutOfStockInAllOnlineLocations bool          `json:"is_out_of_stock_in_all_online_locations"`
}

type BundleComponents struct {
	IsAssortment   bool `json:"is_assortment"`
	IsKitMaster    bool `json:"is_kit_master"`
	IsStandardItem bool `json:"is_standard_item"`
	IsComponent    bool `json:"is_component"`
}

type ProductDescription struct {
	Title             string   `json:"title"`
	BulletDescription []string `json:"bullet_description"`
}

type Variation struct {
}

type ContentLabels struct {
	ImageURL string `json:"image_url"`
}

type Images struct {
	BaseURL       string          `json:"base_url"`
	Primary       string          `json:"primary"`
	ContentLabels []ContentLabels `json:"content_labels"`
}

type SalesClassificationNodes struct {
	NodeID string `json:"node_id"`
}

type Enrichment struct {
	Images                   []Images                   `json:"images"`
	SalesClassificationNodes []SalesClassificationNodes `json:"sales_classification_nodes"`
}

type Handling struct {
}

type RecallCompliance struct {
	IsProductRecalled bool `json:"is_product_recalled"`
}

type TaxCategory struct {
	TaxClass  string `json:"tax_class"`
	TaxCodeID int    `json:"tax_code_id"`
	TaxCode   string `json:"tax_code"`
}

type DisplayOption struct {
	IsSizeChart bool `json:"is_size_chart"`
	IsWarranty  bool `json:"is_warranty"`
}

type Fulfillment struct {
	IsPoBoxProhibited      bool   `json:"is_po_box_prohibited"`
	PoBoxProhibitedMessage string `json:"po_box_prohibited_message"`
}

type PackageDimensions struct {
	Weight                 string `json:"weight"`
	WeightUnitOfMeasure    string `json:"weight_unit_of_measure"`
	Width                  string `json:"width"`
	Depth                  string `json:"depth"`
	Height                 string `json:"height"`
	DimensionUnitOfMeasure string `json:"dimension_unit_of_measure"`
}

type EnvironmentalSegmentation struct {
	IsLeadDisclosure bool `json:"is_lead_disclosure"`
}

type Manufacturer struct {
}

type ProductVendors struct {
	ID                string `json:"id"`
	ManufacturerStyle string `json:"manufacturer_style"`
	VendorName        string `json:"vendor_name"`
}

type ItemType struct {
	CategoryType string `json:"category_type"`
	Type         int    `json:"type"`
	Name         string `json:"name"`
}

type ProductClassification struct {
	ProductType     string   `json:"product_type"`
	ProductTypeName string   `json:"product_type_name"`
	ItemTypeName    string   `json:"item_type_name"`
	ItemType        ItemType `json:"item_type"`
}

type ProductBrand struct {
	Brand   string `json:"brand"`
	FacetID string `json:"facet_id"`
}

type Attributes struct {
	GiftWrapable  string `json:"gift_wrapable"`
	HasProp65     string `json:"has_prop65"`
	IsHazmat      string `json:"is_hazmat"`
	MaxOrderQty   int    `json:"max_order_qty"`
	StreetDate    string `json:"street_date"`
	MediaFormat   string `json:"media_format"`
	MerchClass    string `json:"merch_class"`
	MerchClassid  int    `json:"merch_classid"`
	MerchSubclass int    `json:"merch_subclass"`
	ReturnMethod  string `json:"return_method"`
}

type ReturnPolicies struct {
	User         string `json:"user"`
	PolicyDays   string `json:"policyDays"`
	GuestMessage string `json:"guestMessage"`
}

type Item struct {
	Tcin                      string                    `json:"tcin"`
	BundleComponents          BundleComponents          `json:"bundle_components"`
	Dpci                      string                    `json:"dpci"`
	Upc                       string                    `json:"upc"`
	ProductDescription        ProductDescription        `json:"product_description"`
	BuyURL                    string                    `json:"buy_url"`
	Variation                 Variation                 `json:"variation"`
	Enrichment                Enrichment                `json:"enrichment"`
	ReturnMethod              string                    `json:"return_method"`
	Handling                  Handling                  `json:"handling"`
	RecallCompliance          RecallCompliance          `json:"recall_compliance"`
	TaxCategory               TaxCategory               `json:"tax_category"`
	DisplayOption             DisplayOption             `json:"display_option"`
	Fulfillment               Fulfillment               `json:"fulfillment"`
	PackageDimensions         PackageDimensions         `json:"package_dimensions"`
	EnvironmentalSegmentation EnvironmentalSegmentation `json:"environmental_segmentation"`
	Manufacturer              Manufacturer              `json:"manufacturer"`
	ProductVendors            []ProductVendors          `json:"product_vendors"`
	ProductClassification     ProductClassification     `json:"product_classification"`
	ProductBrand              ProductBrand              `json:"product_brand"`
	ItemState                 string                    `json:"item_state"`
	Specifications            []interface{}             `json:"specifications"`
	Attributes                Attributes                `json:"attributes"`
	CountryOfOrigin           string                    `json:"country_of_origin"`
	RelationshipTypeCode      string                    `json:"relationship_type_code"`
	SubscriptionEligible      bool                      `json:"subscription_eligible"`
	Ribbons                   []interface{}             `json:"ribbons"`
	Tags                      []interface{}             `json:"tags"`
	EstoreItemStatusCode      string                    `json:"estore_item_status_code"`
	IsProposition65           bool                      `json:"is_proposition_65"`
	ReturnPolicies            ReturnPolicies            `json:"return_policies"`
	GiftingEnabled            bool                      `json:"gifting_enabled"`
}

type Product struct {
	DeepRedLabels             DeepRedLabels             `json:"deep_red_labels"`
	AvailableToPromiseNetwork AvailableToPromiseNetwork `json:"available_to_promise_network"`
	Item                      Item                      `json:"item"`
}
