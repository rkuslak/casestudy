package models

// ItemPrice struct to hold Item Price results from Mongo
type ItemPrice struct {
	ItemID   int     `json:"item_id" bson:"item_id,omitempty"`
	Price    float64 `json:"price" bson:"item_price,omitempty"`
	Currency string  `json:"currency" bson:"currency,omitempty"`
}
