package main

import (
	"case/log"
	"case/repository"
	"case/routes"
	"net/http"
)

// ResultRepo quick and dirty globals for MVP
// var ResultRepo repository.ResultsProvider

func main() {
	itemsRepo := repository.NewItemPriceRepository("mongodb://root:superSecret@localhost:27017")
	redSkyRepo := repository.NewRedSkyRepo()
	resultRepo := repository.NewResultsProvider(itemsRepo, redSkyRepo)

	r := routes.NewRouter(resultRepo)
	log.InitializeLogging()

	http.ListenAndServe(":8080", r)
}
