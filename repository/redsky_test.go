package repository

import "testing"

func TestRedSkyRepository_GetItemDescription(t *testing.T) {
	repo := NewRedSkyRepo()

	type args struct {
		itemID int
	}
	tests := []struct {
		name    string
		args    args
		want    string
		wantErr bool
	}{
		{
			"Big Lebowski",
			args{13860428},
			"The Big Lebowski (Blu-ray)",
			false,
		},
		{
			"Big Lebowski",
			args{13860427},
			"Conan the Barbarian (dvd_video)",
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := repo.GetItemDescription(tt.args.itemID)
			if (err != nil) != tt.wantErr {
				t.Errorf("RedSkyRepository.GetItemDescription() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("RedSkyRepository.GetItemDescription() = %v, want %v", got, tt.want)
			}
		})
	}
}
