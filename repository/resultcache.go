package repository

import (
	"case/models"
	"fmt"
	"sync"
	"time"
)

type cacheItem struct {
	Data        *models.Result
	LastUpdated time.Time
}

// IsAfter returns true if the passed Time is prior to the one currently set as
// the LastUpdateTime for the item.
func (item *cacheItem) IsAfter(other time.Time) bool {
	return item.LastUpdated.After(other)
}

// SortKey returns the relative sort key for 2 items, comparing the passed item
// object to the current. Returns positive if time for item is after comparator,
// 0 if the two are equal, and -1 if it is before the passed item.
func (item *cacheItem) SortKey(other *cacheItem) int {
	if item.LastUpdated.After(other.LastUpdated) {
		return 1
	}
	if item.LastUpdated == other.LastUpdated {
		return 0
	}
	return -1
}

// ResultCache caches results from DB and API queries, and returns those results
// if found and unexpired or requeries if not
type ResultCache struct {
	*ResultsRepository
	cache   map[int]*cacheItem
	timeout time.Duration
	maxSize int
	lock    *sync.Mutex
}

// NewCachedResults returns a instance of the ResultCache
func NewCachedResults(repo *ResultsRepository, cacheSize int, timeout time.Duration) *ResultCache {
	return &ResultCache{
		repo,
		map[int]*cacheItem{},
		timeout,
		cacheSize,
		&sync.Mutex{},
	}
}

// ResultCacher is a interface for ResultsProviders to provide Price data
type ResultCacher interface {
	UpdateResult(int, *models.Result) error
	GetResult(int) (models.Result, error)
}

func (rc *ResultCache) getLock() {
	rc.lock.Lock()
}

func (rc *ResultCache) unlock() {
	rc.lock.Unlock()
}

// SetCacheResult will attempt to add the passed Result to the cache. Will
// remove any expired entries and attempt
func (rc *ResultCache) setCacheResult(itemID int, result *models.Result) error {
	var oldestItem *cacheItem

	rc.getLock()
	// Remove aged entries
	timeout := time.Now().Add(0 - rc.timeout)
	for item := range rc.cache {
		if !rc.cache[item].IsAfter(timeout) {
			delete(rc.cache, item)
		} else {
			if oldestItem == nil || rc.cache[item].IsAfter(oldestItem.LastUpdated) {
				oldestItem = rc.cache[item]
			}
		}
	}

	// Avoid realloc if possible
	if len(rc.cache) >= rc.maxSize && oldestItem != nil {
		rc.cache[oldestItem.Data.ID] = nil

		oldestItem.Data = result
		oldestItem.LastUpdated = time.Now()
		rc.cache[itemID] = oldestItem
	} else {
		rc.cache[itemID] = &cacheItem{
			result,
			time.Now(),
		}
	}

	rc.unlock()

	return nil
}

// getCacheResult returns the requested ItemID result, or error if not found in cache
func (rc *ResultCache) getCacheResult(itemID int) (models.Result, error) {
	result, ok := rc.cache[itemID]
	if !ok && time.Now().After(result.LastUpdated.Add(rc.timeout)) {
		return models.Result{}, fmt.Errorf("Item Id not found")
	}

	return *result.Data, nil
}

// GetPriceForItemID wraps the core price for item ID search and returns the
// cached result if valid, or caches and returns the result if not
func (rc *ResultCache) GetPriceForItemID(itemID int) (models.Result, error) {
	result, err := rc.getCacheResult(itemID)
	if err == nil {
		return result, nil
	}

	result, err = rc.ResultsRepository.GetPriceForItemID(itemID)
	if err != nil {
		return models.Result{}, err
	}

	rc.setCacheResult(itemID, &result)
	return result, nil

}

// SetPriceForItemID attempt to update the price for the given item id and if
// successful updates the cache to the new value. Returns encountered error if
// unsuccessful
func (rc *ResultCache) SetPriceForItemID(itemID int, newPrice float64, newCurrency string) error {
	err := rc.ResultsRepository.SetPriceForItemID(itemID, newPrice, newCurrency)
	if err != nil {
		return err
	}

	result, err := rc.getCacheResult(itemID)
	if err != nil {
		return err
	}

	result.CurrentPrice.Value = newPrice
	result.CurrentPrice.Currency = newCurrency
	rc.setCacheResult(itemID, &result)
	return nil
}
