package repository

import (
	"case/models"
	"encoding/json"
	"fmt"
	"net/http"
)

// RedSkyResponse is the root of the returned JSON from the RedSky endpoint
type RedSkyResponse struct {
	Product models.Product `json:"product"`
}

// RedSkyRepository contains the business logic for interacting with the RedSky
// API
type RedSkyRepository struct {
}

// 13860428
const (
	urlTemplate = `https://redsky.target.com/v2/pdp/tcin/%d?excludes=taxonomy,price,promotion,bulk_ship,rating_and_review_reviews,rating_and_review_statistics,question_answer_statistics`
)

// NewRedSkyRepo initializes a new repository for the RedSky API
func NewRedSkyRepo() *RedSkyRepository {
	return &RedSkyRepository{}
}

// GetItemDescription returns the description for the give item id, or error if
// not found
func (repo *RedSkyRepository) GetItemDescription(itemID int) (string, error) {
	var response RedSkyResponse
	res, err := http.Get(fmt.Sprintf(urlTemplate, itemID))
	if err != nil {
		return "", err
	}

	if err := json.NewDecoder(res.Body).Decode(&response); err != nil {
		return "", err
	}

	return response.Product.Item.ProductDescription.Title, nil
}
