package repository

import (
	"strings"
	"testing"
)

func TestItemPriceRepository_SetPriceForItemID(t *testing.T) {
	repo := NewItemPriceRepository("mongodb://root:superSecret@localhost:27017")

	type args struct {
		itemID      int
		newPrice    float64
		newCurrency string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"Update existing - CAN",
			args{13860428, 12.50, "CAN"},
			false,
		},
		{
			"Update existing - USD",
			args{13860428, 12.50, "USD"},
			false,
		},
		{
			"Create new - USD",
			args{13860427, 12.50, "USD"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := repo.SetPriceForItemID(tt.args.itemID, tt.args.newPrice, tt.args.newCurrency); (err != nil) != tt.wantErr {
				t.Errorf("ItemPriceRepository.SetPriceForItemID() error = %v, wantErr %v", err, tt.wantErr)
			}

			result, err := repo.GetPriceForItemID(tt.args.itemID)
			if err != nil || result.ItemID != tt.args.itemID || result.Price != tt.args.newPrice || strings.ToUpper(tt.args.newCurrency) != result.Currency {
				t.Errorf("ResultsRepository.SetPriceForItemID() error = %v, wantErr %v", result, tt.args)
			}
		})
	}
}
