package repository

import (
	"case/log"
	"case/models"
	"context"
	"fmt"
	"strings"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// ItemPriceRepository is a struct for connecting to and resoning with a MongoDB
// instace
type ItemPriceRepository struct {
	ConnectionString string
}

// NewItemPriceRepository returns a new instace of the ItemPriceRepository
func NewItemPriceRepository(connectionString string) *ItemPriceRepository {
	return &ItemPriceRepository{
		ConnectionString: connectionString,
	}
}

func (repo *ItemPriceRepository) getConnection() (*mongo.Collection, error) {
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(repo.ConnectionString))
	if err != nil || client.Ping(context.Background(), readpref.Primary()) != nil {
		return nil, fmt.Errorf("Unable to connect to mongo")
	}

	return client.Database("casestudies").Collection("example"), nil
}

// SetPriceForItemID attempt to save the given price for an item id. returns error on failure
func (repo *ItemPriceRepository) SetPriceForItemID(itemID int, newPrice float64, newCurrency string) error {
	coll, err := repo.getConnection()
	if err != nil {
		log.Log.Info("Unable to open connection to Mongo DB")
		panic("No connection")
	}

	document := models.ItemPrice{
		ItemID:   itemID,
		Price:    newPrice,
		Currency: strings.ToUpper(newCurrency),
	}

	count, err := coll.CountDocuments(context.Background(), bson.D{{Key: "item_id", Value: itemID}})
	if err == nil && count != 0 {
		filter := bson.D{{Key: "item_id", Value: itemID}}
		_, err := coll.UpdateOne(context.Background(), filter, bson.D{{Key: "$set", Value: document}})
		if err != nil {
			log.Log.Errorf("Failed to insert new record: %s", err)
			return err
		}
		log.Log.Infof("Updated record %d\n", itemID)
		return nil
	}

	id, err := coll.InsertOne(context.TODO(), document)
	if err != nil {
		log.Log.Errorf("Failed to insert new record: %s", err)
		return err
	}
	log.Log.Info("Inserted record %s\n", id)

	return nil
}

// GetPriceForItemID returns the price stored for an item, or error if not found
func (repo *ItemPriceRepository) GetPriceForItemID(itemID int) (models.ItemPrice, error) {
	var result models.ItemPrice
	coll, err := repo.getConnection()
	if err != nil {
		log.Log.Errorf("Failed to find record: %s", err)
		return models.ItemPrice{}, err
	}

	if err = coll.FindOne(context.TODO(), bson.D{{Key: "item_id", Value: itemID}}).Decode(&result); err == nil {
		return result, nil
	}

	log.Log.Errorf("Failed to find record: %d", itemID)
	return models.ItemPrice{}, fmt.Errorf("Record not found")
}
