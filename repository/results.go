package repository

import (
	"case/models"
	"fmt"
)

// ResultsRepository contains the business logic to interact with the RedSky and
// Item Price APIs
type ResultsRepository struct {
	priceRepo  *ItemPriceRepository
	redSkyRepo *RedSkyRepository
}

// ResultsProvider is a interface to the expected actions from the
// ResultsRepository, or any equivilent actors
type ResultsProvider interface {
	GetPriceForItemID(int) (models.Result, error)
	SetPriceForItemID(int, float64, string) error
}

// NewResultsProvider creates a new instance of the repository
func NewResultsProvider(priceRepo *ItemPriceRepository, redSkyRepo *RedSkyRepository) *ResultsRepository {
	return &ResultsRepository{
		priceRepo,
		redSkyRepo,
	}
}

// GetPriceForItemID attempts to return the price information for a given
// product id, or error if not found
func (repo *ResultsRepository) GetPriceForItemID(itemID int) (models.Result, error) {
	desc, err := repo.redSkyRepo.GetItemDescription(itemID)
	if err != nil {
		return models.Result{}, err
	}
	if desc == "" {
		return models.Result{}, fmt.Errorf("No result found")
	}

	item, err := repo.priceRepo.GetPriceForItemID(itemID)
	if err != nil {
		return models.Result{}, err
	}

	response := models.Result{
		ID:   itemID,
		Name: desc,
		CurrentPrice: models.Price{
			Value:    item.Price,
			Currency: item.Currency,
		},
	}
	return response, nil
}

// SetPriceForItemID sets the price for a given product id, or return error if
// unsuccessful
func (repo *ResultsRepository) SetPriceForItemID(itemID int, price float64, currency string) error {
	if repo.priceRepo.SetPriceForItemID(itemID, price, currency) == nil {
		return nil
	}

	return fmt.Errorf("Save failed")
}
