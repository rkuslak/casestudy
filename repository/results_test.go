package repository

import (
	"case/models"
	"strings"
	"testing"
)

func TestResultsRepository_GetPriceForItemID(t *testing.T) {
	priceRepo := NewItemPriceRepository("mongodb://root:superSecret@localhost:27017")
	redSkyRepo := NewRedSkyRepo()
	repo := NewResultsProvider(priceRepo, redSkyRepo)

	type args struct {
		itemID int
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			name:    "Successful item test",
			args:    args{itemID: 13860428},
			wantErr: false,
		},
		{
			name:    "Unsuccessful item test",
			args:    args{itemID: 13860426},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := repo.GetPriceForItemID(tt.args.itemID)
			if (err != nil) != tt.wantErr {
				t.Errorf("ResultsRepository.GetPriceForItemID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if tt.wantErr != (got.ID == 0 || got.Name == "" || got.CurrentPrice == models.Price{}) {
				t.Errorf("ResultsRepository.GetPriceForItemID() = %v when wantErr was  %v", got, tt.wantErr)
			}
		})
	}
}

func TestResultsRepository_SetPriceForItemID(t *testing.T) {
	priceRepo := NewItemPriceRepository("mongodb://root:superSecret@localhost:27017")
	redSkyRepo := NewRedSkyRepo()
	repo := NewResultsProvider(priceRepo, redSkyRepo)

	type args struct {
		itemID   int
		price    float64
		currency string
	}
	tests := []struct {
		name    string
		args    args
		wantErr bool
	}{
		{
			"CAN test",
			args{13860428, 12.50, "CAN"},
			false,
		},
		{
			"USD test",
			args{13860428, 12.50, "UsD"},
			false,
		},
		{
			"No change test",
			args{13860428, 10, "UsD"},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := repo.SetPriceForItemID(tt.args.itemID, tt.args.price, tt.args.currency); (err != nil) != tt.wantErr {
				t.Errorf("ResultsRepository.SetPriceForItemID() error = %v, wantErr %v", err, tt.wantErr)
			}
			result, err := repo.GetPriceForItemID(tt.args.itemID)
			if err != nil || result.ID != tt.args.itemID || result.CurrentPrice.Value != tt.args.price || strings.ToUpper(tt.args.currency) != result.CurrentPrice.Currency {
				t.Errorf("ResultsRepository.SetPriceForItemID() error = %v, wantErr %v", result, tt.args)
			}
		})
	}
}
